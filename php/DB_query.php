<?php

function registration($e_mail, $password){
    global $db;
    $addUserQuery = $db->prepare('
        INSERT INTO users(e_mail, password)
        VALUES (:e_mail, :password)
        ');
    $addUserQuery->execute([
        'e_mail'   =>$e_mail,
        'password' => $password
    ]);
}

function checkEmail($e_mail){
    global $db;
    $errors = [];
    $e_mail_exist = false;

    $checkEmailQuery = $db ->prepare("
        SELECT id_user, e_mail, password
        FROM users
        WHERE e_mail = :e_mail 
        ");
    $checkEmailQuery->execute([
        'e_mail' => $e_mail,
    ]);
    foreach ($checkEmailQuery as $users){
        if ($users) {
            $errors['e_mail_exist'] = true;
        }
    }
    $_SESSION['errors'] =  $errors;
}
