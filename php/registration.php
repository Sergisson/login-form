<?php
//TODO Keep your password in a secure fashion
require_once 'DB_init.php';
require_once 'DB_query.php';

$errors = isset($_SESSION['errors']) ? $_SESSION['errors'] : [];

if (isset($_POST['e_mail'], $_POST['password'])) {
    $fields = [
        'e_mail_input'   => strtolower(trim($_POST['e_mail'])),
        'password_input' => trim($_POST['password'])
    ];

    foreach ($fields as $field => $data) {//true = error
        if (empty($data)) {
            $errors[$field] = true;
        }
    }
    if (!preg_match('@[A-z]@u', $fields['e_mail_input'])) { //Regular expression for find English char TODO цифры забыл :(
        $errors['A-z'] = true;
    }

    if (!strpbrk($fields['e_mail_input'], '@')) {
        $errors['@'] = true;
    }
    if (strpbrk($fields['e_mail_input'], ' ')) {
        $errors['e_mail_space'] = true;
    }
    if (strpbrk($fields['password_input'], ' ')) {
        $errors['password_space'] = true;
    }

    if (empty($errors)) { //Why don't work 'is_null'?
        checkEmail($fields['e_mail_input']);
        registration($fields['e_mail_input'], $fields['password_input']);
        //header('Location: ../Login_page.php');
    }
}
$_SESSION['errors'] = $errors;

header('Location: ../Registration_page.php');
