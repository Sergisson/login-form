<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login Form</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<div class="background">
    <div class="login">
        <span class="login_header"><h1>Login</h1></span>
        <div class="login_body">
            <form action="php/sign_in.php" method="post">
                <label>
                    <p class="text_login">E-mail adress</p>
                    <input type="text" name="e_mail" src="icon/icon-person.png"> <!--!!!icon-->
                </label>
                <label>
                    <p class="text_login">Password</p>
                    <input class="input_login" type="text" name="password">
                </label>
                <p><a href="Forgot_password_page.php">Forgot password?</a></p>
                <div class="sign_in_button">
                    <input type="submit" value="Sign in">
                </div>
            </form>
            <div class="registration_button"><a href="Registration_page.php">Registration</a></div>
        </div>
    </div>
</div>

</body>
</html>