<?php
session_start();
$errors = isset($_SESSION['errors']) ? $_SESSION['errors'] : [];
$fields = isset($_SESSION['fields']) ? $_SESSION['fields'] : [];

var_dump($errors);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login Form</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<div class="background">
    <div class="login">
        <div class="login_header"><h1>Registration</h1></div>
        <div class="login_body">
            <form action="php/registration.php" method="post">
                <label>
                    <p class="text_login">E-mail adress</p> <!--TODO Don't work: substituting the values of the field -->
                    <input type="text" name="e_mail"
                        <?php echo isset($fields['e_mail_input']) ? ' value = "' . $fields['e_mail_input'] . ' "' : '' ?>>
                    <!--!!!icon src="icon/icon-person.png" --><!--TODO add icon-->
                    </label>

                    <!--switch ($errors) is it possible?
                    TODO e_mail_input=> English_symbol (A-z)?=>@ => e_mail_space=> e_mail_exist=>-->
                    <?php if(!empty (($errors['e_mail_input']))){ ?>
                        <div class="error">Error! E-mail not entered.</div>
                    <?php } else {if (!empty($errors["@"])):  ?>
                        <div class="error">Error! @ Is not written. It's just an email?</div>
                    <?php  endif;} ?>

                    <?php if (!empty($errors['e_mail_exist'])): ?>
                        <div class="error">Error! E-mail is already registered</div>
                    <?php  endif; ?>

                    <!--TODO password_input=>min_symbol=>password_space=>Uppercase symbol=> lower case symbol=> number=>-->
                    <label>
                        <span class="text_login">Password</span>
                        <input class="input_login" type="text" name="password" >
                    </label>
                    <?php if(!empty ($errors['password_input'])): ?>
                        <div class="error">Error! Password not entered.</div>
                    <?php  endif; ?>
                    <div class="registration_button_now">
                        <input type="submit" value="Registration now">
                    </div>
            </form>

        </div>
    </div>
</div>

</body>
</html>

<?php
/*unset($_SESSION['exist']);
unset($_SESSION['errors']);
unset($_SESSION['fields']);*/
session_destroy();
?>

<?php /*if(!empty ($errors['e_mail'])){ */?><!--
    <div class="error">Error! E-mail not entered.</div>
<?php /*} else {if (!empty($errors['@'])):  */?>
    <div class="error">Error! @ Is not written. It's just an email?</div>
<?php /* endif;} */?>

<?php /*if (!empty($_SESSION['exist'])): */?>
    <div class="error">Error! E-mail is already registered</div>
--><?php /* endif; */?>
